﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Veit.JWT.Configuration
{
    public static class JsonWebKeys
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public static Dictionary<string, JwtSettings> Get(string issuer, string path = "/.well-known/jwks.json")
        {
            logger.Debug($"Get json web keys from : {issuer + path}");
            var keys = new Dictionary<string, JwtSettings>();
            using (var wc = new WebClient())
            {
                try
                {
                    var json = wc.DownloadString(issuer + path);
                    var jwks = JsonConvert.DeserializeObject<Jwks>(json);
                    keys = jwks.Keys.ToDictionary(j => j.KeyId, v => new JwtSettings { Expo = v.Exponent, Issuer = issuer, Key = v.Modulus });
                }
                catch (Exception e)
                {
                    logger.Error(e, "Failed to load json web keys.");
                }
            }
            return keys;
        }
    }

    internal class Jwks
    {
        [JsonProperty(PropertyName = "keys")]
        public List<Jwk> Keys { get; set; }
    }

    internal class Jwk
    {
        [JsonProperty(PropertyName = "alg")]
        public string Algorithm { get; set; }

        [JsonProperty(PropertyName = "e")]
        public string Exponent { get; set; }

        [JsonProperty(PropertyName = "kid")]
        public string KeyId { get; set; }

        [JsonProperty(PropertyName = "kty")]
        public string KeyType { get; set; }

        [JsonProperty(PropertyName = "n")]
        public string Modulus { get; set; }

        [JsonProperty(PropertyName = "use")]
        public string KeyUse { get; set; }
    }
}
